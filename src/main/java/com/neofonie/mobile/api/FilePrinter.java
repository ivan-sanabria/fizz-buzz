/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * Class to print the Fizz - Buzz output in specific file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class FilePrinter {

    /**
     * Define default constructor of FilePrinter.
     */
    FilePrinter() {}

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(FilePrinter.class);

    /**
     * Print the given content in a file with specific name.
     *
     * @param name    Name of the file to print Fizz - Buzz output.
     * @param content Content to print in the file.
     */
    void printOutput(final String name, final String content) {

        try {

            final File file = new File(name);
            final PrintWriter writer = new PrintWriter(file, StandardCharsets.UTF_8);

            writer.append(content);
            writer.close();

            LOG.info("Fizz - Buzz {} generation file is done.", name);

        } catch (IOException ioe) {

            LOG.error("Error writing the file into the disk - ", ioe);
        }
    }

}
