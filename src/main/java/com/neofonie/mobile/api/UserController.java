/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class to handle command operations receiving input by console to generate output on console or a file depending on
 * number of arguments:
 *
 * <ul>
 *  <li> 0 arguments - console fizz buzz output from 1 to 100
 *  <li> 1 argument  - fizz buzz output in a file given as argument from 1 to 100
 *  <li> 2 arguments - console fizz buzz output from first to second argument in ascending order
 *  <li> 3 arguments - output in a file given as last argument and fizz buzz generation from first to second argument
 * in ascending order
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.3.0
 */
public class UserController {

    /**
     * Define default constructor of UserController.
     */
    public UserController() {}

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(UserController.class);

    /**
     * Executes the application to generate Fizz - Buzz output.
     *
     * @param args Arguments used to generate output.
     */
    public static void main(String... args) {

        final FizzBuzzGenerator generator = new FizzBuzzGenerator();

        if (0 == args.length)
            generateConsoleOutput(generator, 1, 100);

        if (1 == args.length)
            generateFileOutput(generator, 1, 100, args[0]);

        if (2 <= args.length && 4 > args.length)
            generateIntervalOutput(generator, args);
    }

    /**
     * Generates the output of Fizz - Buzz for specific interval using the given args and processor.
     *
     * @param generator Processor instance used to generate output.
     * @param args      Arguments used to generate output.
     */
    private static void generateIntervalOutput(final FizzBuzzGenerator generator, final String[] args) {

        try {

            final long first = Long.parseLong(args[0]);
            final long second = Long.parseLong(args[1]);

            final long begin = first > second ? second : first;
            final long end = first > second ? first : second;

            selectIntervalOutput(args, generator, begin, end);

        } catch (NumberFormatException nfe) {

            LOG.error("Error processing the input as range with minimum {} and maximum {} - ", args[0], args[1], nfe);
        }
    }

    /**
     * Select which output is used to display the Fizz - Buzz content for specific interval using the given args and
     * processor.
     *
     * @param args      Arguments used to generate output.
     * @param generator Processor instance used to generate output.
     * @param begin     Start value in specific interval to generate Fizz - Buzz content.
     * @param end       End value in specific interval to generate Fizz - Buzz content.
     */
    private static void selectIntervalOutput(final String[] args, final FizzBuzzGenerator generator, final long begin,
                                             final long end) {

        if (3 == args.length)
            generateFileOutput(generator, begin, end, args[2]);
        else
            generateConsoleOutput(generator, begin, end);

    }

    /**
     * Generates the output of Fizz - Buzz content for specific interval and printed into a file.
     *
     * @param generator Processor instance used to generate output.
     * @param begin     Start value in specific interval to generate Fizz - Buzz content.
     * @param end       End value in specific interval to generate Fizz - Buzz content.
     * @param filename  Filename to store the Fizz-Buzz content.
     */
    private static void generateFileOutput(final FizzBuzzGenerator generator, final long begin, final long end,
                                           final String filename) {

        final FilePrinter printer = new FilePrinter();
        final String text = generator.generateOutputText(begin, end);

        printer.printOutput(filename, text);
    }

    /**
     * Generates the output of Fizz - Buzz content for specific interval and printed into the console.
     *
     * @param generator Processor instance used to generate output.
     * @param begin     Start value in specific interval to generate Fizz - Buzz content.
     * @param end       End value in specific interval to generate Fizz - Buzz content.
     */
    private static void generateConsoleOutput(final FizzBuzzGenerator generator, final long begin, final long end) {

        final String text = generator.generateOutputText(begin, end);

        System.out.println(text);
    }

}
