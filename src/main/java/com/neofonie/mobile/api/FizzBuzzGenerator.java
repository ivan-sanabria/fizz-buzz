/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.api;

/**
 * Class to process the a interval of numbers to generate the Fizz - Buzz output.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.3.0
 */
public class FizzBuzzGenerator {

    /**
     * Define default constructor of FizzBuzzGenerator.
     */
    public FizzBuzzGenerator() {}

    /**
     * Fizz module factor to replace the number with Fizz.
     */
    public static final int FIZZ_MODULE = 3;

    /**
     * Buzz module factor to replace the number with Buzz.
     */
    public static final int BUZZ_MODULE = 5;

    /**
     * Generate the output of Fizz - Buzz for the given number interval.
     *
     * @param start Minimum number of the interval.
     * @param end   Maximum number of the interval.
     * @return A String representing the Fizz - Buzz content of the interval.
     */
    public String generateOutputText(final long start, final long end) {

        final String separator = System.lineSeparator();
        final StringBuilder sb = new StringBuilder();

        for (long a = start; a <= end; a++) {
            appendFizzBuzz(sb, a);
            sb.append(separator);
        }

        return sb.toString();
    }

    /**
     * Add Fizz, Buzz or the given a value to the given builder.
     *
     * @param sb String builder containing Fizz - Buzz output.
     * @param a  Current number to determine the next value to append to Fizz - Buzz builder.
     */
    private void appendFizzBuzz(final StringBuilder sb, final long a) {

        if ((a % FIZZ_MODULE != 0) && (a % BUZZ_MODULE != 0))
            sb.append(a);


        if (a % FIZZ_MODULE == 0)
            sb.append("Fizz");


        if (a % BUZZ_MODULE == 0)
            sb.append("Buzz");
    }

}
