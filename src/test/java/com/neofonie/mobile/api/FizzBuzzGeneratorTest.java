/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.api;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Class to handle the test cases for generating Fizz Buzz content.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.3.0
 */
public class FizzBuzzGeneratorTest {

    /**
     * Define line separator to add on assertions.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define instance for testing purposes.
     */
    private final FizzBuzzGenerator generator = new FizzBuzzGenerator();


    @Test
    public void generate_from_minus_five_to_minus_ten() {

        final String output = generator.generateOutputText(-5, -10);
        assertTrue(output.isEmpty());
    }

    @Test
    public void generate_from_minus_ten_to_minus_five() {

        final String expectedOutput = "Buzz" + LINE_SEPARATOR
                + "Fizz" + LINE_SEPARATOR
                + "-8" + LINE_SEPARATOR
                + "-7" + LINE_SEPARATOR
                + "Fizz" + LINE_SEPARATOR
                + "Buzz" + LINE_SEPARATOR;

        final String output = generator.generateOutputText(-10, -5);
        assertEquals(expectedOutput, output);
    }

    @Test
    public void generate_from_minus_five_to_zero() {

        final String expectedOutput = "Buzz" + LINE_SEPARATOR
                + "-4" + LINE_SEPARATOR
                + "Fizz" + LINE_SEPARATOR
                + "-2" + LINE_SEPARATOR
                + "-1" + LINE_SEPARATOR
                + "FizzBuzz" + LINE_SEPARATOR;

        final String output = generator.generateOutputText(-5, 0);
        assertEquals(expectedOutput, output);
    }

    @Test
    public void generate_from_zero_to_zero() {

        final String expectedOutput = "FizzBuzz" + LINE_SEPARATOR;

        final String output = generator.generateOutputText(0, 0);
        assertEquals(expectedOutput, output);
    }

    @Test
    public void generate_from_zero_to_five() {

        final String expectedOutput = "FizzBuzz" + LINE_SEPARATOR
                + "1" + LINE_SEPARATOR
                + "2" + LINE_SEPARATOR
                + "Fizz" + LINE_SEPARATOR
                + "4" + LINE_SEPARATOR
                + "Buzz" + LINE_SEPARATOR;

        final String output = generator.generateOutputText(0, 5);
        assertEquals(expectedOutput, output);
    }

    @Test
    public void generate_from_five_to_ten() {

        final String expectedOutput = "Buzz" + LINE_SEPARATOR
                + "Fizz" + LINE_SEPARATOR
                + "7" + LINE_SEPARATOR
                + "8" + LINE_SEPARATOR
                + "Fizz" + LINE_SEPARATOR
                + "Buzz" + LINE_SEPARATOR;

        final String output = generator.generateOutputText(5, 10);
        assertEquals(expectedOutput, output);
    }

    @Test
    public void generate_from_ten_to_five() {

        final String output = generator.generateOutputText(10, 5);
        assertTrue(output.isEmpty());
    }

}
