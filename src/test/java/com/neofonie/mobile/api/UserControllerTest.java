/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.api;

import com.neofonie.mobile.util.OutputValidator;
import org.testng.annotations.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * Class to handle the test cases using a specific inputs from static values and validating the existence of the txt after
 * the query is made.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.3.0
 */
public class UserControllerTest {


    @Test
    public void generate_fizz_buzz_from_1_to_100_to_console() {

        final String[] args = {};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        UserController.main(args);
        OutputValidator.validateOutputStream(1, controllerOut);
    }

    @Test
    public void generate_fizz_buzz_from_1_to_100_to_file() {

        final String fileName = "test-file";
        final String[] args = {fileName};

        UserController.main(args);
        OutputValidator.validateTestFile(fileName, 1, 100);
    }

    @Test
    public void generate_fizz_buzz_from_a_to_b_to_console() {

        final String[] args = {"a", "b"};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        UserController.main(args);

        final String output = controllerOut.toString();
        assertTrue(output.isEmpty());
    }

    @Test
    public void generate_fizz_buzz_from_a_to_b_to_file() {

        final String fileName = "test-file.txt";
        final String[] args = {"a", "b", fileName};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        UserController.main(args);

        final File testFile = new File(fileName);
        assertFalse(testFile.exists());
    }

    @Test
    public void generate_fizz_buzz_from_20000_to_20100_to_console() {

        final String[] args = {"20000", "20100"};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        UserController.main(args);
        OutputValidator.validateOutputStream(20000, controllerOut);
    }

    @Test
    public void generate_fizz_buzz_from_20000_to_20100_to_file() {

        final String fileName = "test-file.txt";
        final String[] args = {"20000", "20100", fileName};

        UserController.main(args);
        OutputValidator.validateTestFile(fileName, 20000, 20100);
    }

    @Test
    public void generate_fizz_buzz_from_20000_to_10100_to_console() {

        final String[] args = {"20000", "10100"};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        UserController.main(args);
        OutputValidator.validateOutputStream(10100, controllerOut);
    }

    @Test
    public void generate_fizz_buzz_from_20000_to_10100_to_file() {

        final String fileName = "test-file.txt";
        final String[] args = {"20000", "10100", fileName};

        UserController.main(args);
        OutputValidator.validateTestFile(fileName, 10100, 20000);
    }

    @Test
    public void generate_fizz_buzz_with_too_much_args_to_console() {

        final String[] args = {"a", "b", "c", "d"};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        UserController.main(args);
        assertTrue(controllerOut.toString().isEmpty());
    }

    @Test
    @SuppressWarnings("AccessStaticViaInstance")
    public void generate_fizz_buzz_from_1_to_100_by_instance() {

        final String[] args = {};
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setOut(new PrintStream(controllerOut));

        final UserController userController = new UserController();
        userController.main(args);

        OutputValidator.validateOutputStream(1, controllerOut);
    }

}
