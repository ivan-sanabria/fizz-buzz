/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.api;

import com.neofonie.mobile.util.OutputValidator;
import org.testng.annotations.Test;

/**
 * Class to handle the test cases for writing specific content to a file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.3.0
 */
public class FilePrinterTest {

    /**
     * Define instance for testing purposes.
     */
    private final FilePrinter filePrinter = new FilePrinter();


    @Test(expectedExceptions = NullPointerException.class)
    public void print_to_null_file_name_with_null_content() {

        filePrinter.printOutput(null, null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void print_to_null_file_name_with_empty_content() {

        final String content = "";

        filePrinter.printOutput(null, content);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void print_to_null_file_name_with_valid_content() {

        final String content = "valid content";

        filePrinter.printOutput(null, content);
    }

    @Test
    public void print_to_empty_file_name_with_null_content() {

        final String fileName = "";

        filePrinter.printOutput(fileName, null);
        OutputValidator.validateTestFile(fileName, "null");
    }

    @Test
    public void print_to_empty_file_name_with_empty_content() {

        final String fileName = "";
        final String content = "";

        filePrinter.printOutput(fileName, content);
        OutputValidator.validateTestFile(fileName, content);
    }

    @Test
    public void print_to_empty_file_name_with_valid_content() {

        final String fileName = "";
        final String content = "valid content";

        filePrinter.printOutput(fileName, content);
        OutputValidator.validateTestFile(fileName, content);
    }

    @Test
    public void print_to_valid_file_name_with_null_content() {

        final String fileName = "test-valid-1.txt";

        filePrinter.printOutput(fileName, null);
        OutputValidator.validateTestFile(fileName, "null");
    }

    @Test
    public void print_to_valid_file_name_with_empty_content() {

        final String fileName = "test-valid-2.txt";
        final String content = "";

        filePrinter.printOutput(fileName, content);
        OutputValidator.validateTestFile(fileName, content);
    }

    @Test
    public void print_to_valid_file_name_with_valid_content() {

        final String fileName = "test-valid-3.txt";
        final String content = "valid content";

        filePrinter.printOutput(fileName, content);
        OutputValidator.validateTestFile(fileName, content);
    }

}
