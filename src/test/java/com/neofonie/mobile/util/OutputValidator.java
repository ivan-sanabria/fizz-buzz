/*
 * Copyright (C) 2012 Iván Camilo Sanabria.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neofonie.mobile.util;

import com.neofonie.mobile.api.FizzBuzzGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Class to collect extra logic required to validate outputs of the application.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.3.0
 */
public class OutputValidator {

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(OutputValidator.class);

    /**
     * Validate the OutputStream given on the console.
     *
     * @param start         Start of the interval to generate FizzBuzz output.
     * @param controllerOut OutputStream with the content of the console redirected by each test case.
     */
    public static void validateOutputStream(long start, final ByteArrayOutputStream controllerOut) {

        final String consoleOutput = controllerOut.toString();

        final String[] lines = consoleOutput.split(LINE_SEPARATOR);
        assertTrue(0 != lines.length);

        for (String line : lines) {
            validateOutputLine(start, line);
            start++;
        }
    }

    /**
     * Validate the test file is created and has the proper content.
     *
     * @param filename Filename of the test file.
     * @param start    Start value in specific interval to generate Fizz - Buzz content.
     * @param end      End value in specific interval to generate Fizz - Buzz content.
     */
    public static void validateTestFile(final String filename, final long start, final long end) {

        final FizzBuzzGenerator generator = new FizzBuzzGenerator();
        final String expectedOutput = generator.generateOutputText(start, end);

        validateTestFile(filename, expectedOutput);
    }

    /**
     * Validate the test file is created and has the proper content.
     *
     * @param filename Filename of the test file.
     * @param content  Expected content of the file.
     */
    public static void validateTestFile(final String filename, final String content) {

        final File file = new File(filename);

        try {

            assertContentTestFile(file, content);
            boolean delete = file.delete();

            LOG.debug("File deleted from test case {}.", delete);

        } catch (IOException ioe) {

            LOG.error("Error reading the test file  - ", ioe);
        }
    }

    /**
     * Asserts that the given file contains the given content.
     *
     * @param file    File generated over a test case.
     * @param content Content of the file to assert.
     * @throws IOException Thrown when the file does not exists.
     */
    private static void assertContentTestFile(final File file, final String content) throws IOException {

        final String[] expectedLines = content.split(LINE_SEPARATOR);
        final Scanner sc = new Scanner(file);

        int index = 0;

        while (sc.hasNextLine()) {

            final String current = sc.nextLine();
            assertEquals(expectedLines[index], current);

            index++;
        }

        sc.close();
    }

    /**
     * Validate that the given line asserts with the expected output for the given number.
     *
     * @param number Expected number to validate expected output.
     * @param line   Line capture on file or console for validation.
     */
    private static void validateOutputLine(final long number, final String line) {

        if (number % FizzBuzzGenerator.FIZZ_MODULE != 0 && number % FizzBuzzGenerator.BUZZ_MODULE != 0)
            assertEquals(line, String.valueOf(number));

        if (number % FizzBuzzGenerator.FIZZ_MODULE == 0)
            assertTrue(line.contains("Fizz"));

        if (number % FizzBuzzGenerator.BUZZ_MODULE == 0)
            assertTrue(line.contains("Buzz"));
    }

}
