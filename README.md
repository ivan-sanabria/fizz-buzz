# Fizz Buzz - Interview Question 

version 2.7.0 - 21/01/2023

[![License](https://img.shields.io/badge/license-apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/fizz-buzz.svg)](http://bitbucket.org/ivan-sanabria/fizz-buzz/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/fizz-buzz.svg)](http://bitbucket.org/ivan-sanabria/fizz-buzz/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_fizz-buzz&metric=alert_status)](https://sonarcloud.io/project/overview?id=ivan-sanabria_fizz-buzz)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_fizz-buzz&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=ivan-sanabria_fizz-buzz)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_fizz-buzz&metric=coverage)](https://sonarcloud.io/component_measures?id=ivan-sanabria_fizz-buzz&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_fizz-buzz&metric=ncloc)](https://sonarcloud.io/code?id=ivan-sanabria_fizz-buzz)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_fizz-buzz&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=ivan-sanabria_fizz-buzz)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=ivan-sanabria_fizz-buzz&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=ivan-sanabria_fizz-buzz)

## Introduction

Test question designed to filter programming candidates over interviews. The assignment of the test is:

*"Write a program that prints the numbers from 1 to 100. But for multiples of three print **Fizz** instead of the number 
and for the multiples of five print **Buzz**. For numbers which are multiples of both three and five print **FizzBuzz**."* 

The test also should cover the following scenarios:

1. The program does not have arguments should print the **"Fizz Buzz"** data in console.
2. The program has one argument. Assume the argument is the name of a file to store the **"Fizz Buzz"** data.
3. The program has two arguments. Assume the first one is a start number and the second one is a end number of a range 
to generate the **"Fizz Buzz"** data in console.
4. The program has three arguments. Assume the case 3 and 2 in the given order.
5. The program has more than 3 arguments. Ignore all of them.

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **UserController.java**.

## Running Application on Terminal

To run the application on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean compile assembly:single
    java -jar target/fizz-buzz-2.7.0.jar 10 100 local-test.txt
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Example of Supported Commands

These are the supported commands of the application:

```bash
    # Generate fizz buzz sequence from 1 to 100 in console
    java -jar target/fizz-buzz-2.7.0.jar
    # Generate fizz buzz sequence from 1 to 100 and store in the given file name.
    java -jar target/fizz-buzz-2.7.0.jar local-test.txt
    # Generate fizz buzz sequence from 10 to 100 in console
    java -jar target/fizz-buzz-2.7.0.jar 10 100
    # Generate fizz buzz sequence from 10 to 100 and store in the given file name.
    java -jar target/fizz-buzz-2.7.0.jar 10 100 local-test.txt
```

# Contact Information

Email: icsanabriar@googlemail.com